const menuBtn = document.querySelector("#menu-btn");
const menuList = document.querySelector("#menu-list");
menuBtn.addEventListener("click", () => {
  menuBtn.classList.toggle("active");
  menuList.classList.toggle("active");
  createOverlay();
});

function createOverlay() {
  if (menuBtn.classList.contains("active")) {
    const overlay = document.createElement("div");
    overlay.classList.add("overlay");
    document.body.append(overlay);
    overlay.addEventListener("click", (e) => {
      menuBtn.classList.remove("active");
      menuList.classList.remove("active");
      overlay.remove();
    });
  }
}
