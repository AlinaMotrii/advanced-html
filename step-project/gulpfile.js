import gulp from "gulp";
import dartSass from "sass";
import gulpSass from "gulp-sass";
import cssMin from "gulp-clean-css";
import terser from "gulp-terser";
import concat from "gulp-concat";
import gulpImagemin from "gulp-imagemin";
import bs from "browser-sync";
import clean from "gulp-clean";
import { existsSync } from "fs";

const sass = gulpSass(dartSass);
const browserSync = bs.create();

// CLEAN FUNCTIONS ------------------------------------------
const cleanDist = () => {
  return gulp.src("./dist", { read: false }).pipe(clean({ allowEmpty: true }));
};

const cleanCss = () => {
  return gulp.src("./dist/css", { read: false }).pipe(clean());
};

const cleanJs = () => {
  return gulp
    .src("./dist/js", { read: false })
    .pipe(clean({ allowEmpty: true }));
};

const cleanImg = () => {
  return gulp
    .src("./dist/img", { read: false })
    .pipe(clean({ allowEmpty: true }));
};

// STYLES, JS, IMAGES ------------------------------------------
const scss = () => {
  return gulp
    .src("./src/scss/**/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(cssMin())
    .pipe(gulp.dest("./dist/css"));
};

const js = () => {
  return gulp
    .src("./src/js/**/*.js")
    .pipe(concat("index.js"))
    .pipe(terser())
    .pipe(gulp.dest("./dist/js"));
};

const img = () => {
  return gulp
    .src("./src/img/**/*")
    .pipe(gulpImagemin())
    .pipe(gulp.dest("./dist/img"));
};

// TASKS ------------------------------------------
const build = gulp.parallel(scss, js, img);

const dev = () => {
  browserSync.init({
    server: {
      baseDir: "./",
    },
  });

  gulp.watch("./*.html", (next) => {
    browserSync.reload();
    next();
  });

  gulp.watch(
    "./src/scss/**/*.scss",
    gulp.series(scss, (next) => {
      browserSync.reload();
      next();
    })
  );

  gulp.watch(
    "./src/js/**/*js",
    gulp.series(cleanJs, js, (next) => {
      browserSync.reload();
      next();
    })
  );

  gulp.watch(
    "./src/img/**/*",
    gulp.series(cleanImg, img, (next) => {
      browserSync.reload();
      next();
    })
  );
};

gulp.task("build", gulp.series(cleanDist, build));
gulp.task("dev", gulp.series(build, dev));
